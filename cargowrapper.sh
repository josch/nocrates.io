#!/bin/sh

set -eu

if [ ! -e Cargo.toml ]; then
	echo "E: cannot find Cargo.toml" >&2
	exit 1
fi

if [ "$#" -lt 1 ]; then
	echo "E: what to do? Choose one of: build rustc doc test bench install" >&2
	exit 1
fi

cat << END | python3 -
import toml
deps=[]
for k,v in toml.load(open('Cargo.toml'))['dependencies'].items():
	dep = "librust-" + k.replace("_", "-")
	if not hasattr(v, "get") or v.get('default-features', True):
		dep += "+default"
	dep += "-dev"
	deps.append(dep)
print("I: to install dependencies, consider running:")
print(" ".join(["sudo", "apt", "satisfy"]+deps))
END

for pkg in $(python3 -c "import toml;t=toml.load(open('Cargo.toml'));print(' '.join(t['dependencies'].keys()))"); do
	if [ "$(dpkg-query --show --showformat='${db:Status-Status}' librust-$pkg-dev)" != installed ]; then
		echo "I: you may have to install librust-$pkg-dev" >&2
	fi
done

# this setting should deny all network access. Another way to make sure is to
# unshare the network namespace with `firejail --noprofile --net=none`
export CARGO_NET_OFFLINE=true
export CARGO_HOME=$(pwd)/debian/cargo_home
export DEB_CARGO_CRATE=$(python3 -c "import toml;t=toml.load(open('Cargo.toml'));print(t['package']['name']+'_'+t['package']['version'])")
export PATH=/usr/share/cargo/bin:$PATH
eval "$(dpkg-buildflags --export=sh)"
export DEB_HOST_RUST_TYPE=$(printf "include /usr/share/rustc/architecture.mk\n\nall:\n\techo \$(DEB_HOST_RUST_TYPE)" | make --no-print-directory -sf -)
eval $(dpkg-architecture --print-set)

if [ ! -e debian/cargo_home ] || [ ! -e debian/cargo_registry ]; then
	cargo prepare-debian debian/cargo_registry --link-from-system
fi
cargo "$@"
