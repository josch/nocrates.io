A wrapper around /usr/share/cargo/bin/cargo to allow using cargo without
accessing the network. This means, that cargo will use the sources from Debian
libcargo-*-dev packages instead of downloading them from crates.io.

To verify (and make sure) that the script really doesn't access the network,
you can wrap it in `firejail --noprofile --net=none`.
